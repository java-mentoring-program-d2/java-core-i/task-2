public class DynamicDoubleArray {
    private double[] array;
    private int capacity;
    private int size;

    public DynamicDoubleArray(int capacity) {
        this.capacity = capacity;
        array = new double[capacity];
        this.size = 0;
    }

    public DynamicDoubleArray() {
        this(10);
    }

    public static void main(String[] args) {
        DynamicDoubleArray array = new DynamicDoubleArray(16);
        for (int i = 1; i <= 20; i++) {
            array.add(i);
        }
        System.out.println(array);
        System.out.println(array.get(5));
        System.out.println(array.size());

        array.remove(9);
        array.remove(9);
        array.remove(9);
        array.remove(9);
        System.out.println(array);
        System.out.println(array.size());
    }

    public int size() {
        return size;
    }

    public void add(double element) {
        if (size == capacity) {
            capacity = (int) (capacity * 1.5);
            double[] oldArray = array;
            array = new double[capacity];
            System.arraycopy(oldArray, 0, array, 0, size);
        }
        array[++size - 1] = element;
    }

    public double get(int index) {
        if (index >= size) {
            throw new IndexOutOfBoundsException();
        }
        return array[index];
    }

    public void remove(int index) {
        if (index >= size) {
            throw new IndexOutOfBoundsException();
        } else if (index == size - 1) {
            array[size - 1] = 0.0;
        } else {
            System.arraycopy(array, index + 1, array, index, size - 1 - index);
            array[--size] = 0.0;
        }

    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder("DynamicArray of " + double.class + " : {");
        for (int i = 0; i < size; i++) {
            builder.append(array[i]);
            if ((i == size - 1)) {
                builder.append("}\n");
            } else {
                builder.append(", ");
            }
        }
        return builder.toString();
    }
}
